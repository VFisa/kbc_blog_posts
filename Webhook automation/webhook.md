![Triggering KBC orchestration with webhook](images/intro_logo.png)

# How to trigger orchestration by form submission

## Use case
Keboola just implemented [product assessment tool](https://www.keboola.com/product-assessment) dedicated to OEM partners. Form will show how they fare in the various dimensions of data product readiness, the areas on which to focus, and the specific next steps to undertake. 

We wanted to trigger the orchestration that extract the response (have you noticed new Typeform extractor?), processes the data and updates our GoodData dashboard with answers. There was no option to use ["Magic Button"](http://wiki.keboola.com/pure-gooddata-hints/gooddata-widgets) to do so because there is no guarantee the respondent would click on it at the end of the form. 

The easiest way was to create a Zapier "zap" with the webhook:   
![zap with Typeform and webhook](images/webhook.png)

You select which form:   
![Typeform form](images/form.png)

<<<<<<< HEAD
And what to do:   
![Typeform action](images/action.png)

=======
>>>>>>> 9c139436450c639c4243958424740b638b92ba01
What will create a trigger:   
![Typeform trigger](images/trigger.png)

And what to do:   
![Typeform form](images/action.png)

Tricky part might be a configuration of the webhook, it is simply a conversion of [API call](http://docs.keboolaorchestratorv2api.apiary.io/). You need to fill the orchestration ID and in advanced config section your storage token:
![Webhook configuration](images/webhook_config_comment.png)

Here is the full flow:   
![Zapier flow](images/flow.png)

At the end, you have a setup ready, so every time someone fills the form, new data is extracted and your GoodData dashboard is updated.


## More stuff

So go and check out our Typeform extractor:   
![Typeform extractor](images/extractor.png)

FYI you can also create those simple "zaps" between Typeform and Slack:   
![Typeform Slack Zap](images/Typeform-Slack.png)

## Contact
Fisa AT Keboola.com    
Vancouver, BC    
https://bitbucket.org/VFisa/   
