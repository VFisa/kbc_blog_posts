

```python
## import libraries
import pandas as pd
import numpy as np
import os
from os import listdir
```


```python
## give me all files in the folder
print(os.listdir("/data/in/tables/"))
```

    ['out.c-main.forecast_sales.csv', 'out.c-main.forecast_sales.csv.manifest']



```python
## this is the file then
file = "/data/in/tables/out.c-main.forecast_sales.csv"
```


```python
## create dataframe object from the data
model = pd.read_csv(file)
```


```python
## whats up now
model.columns
```




    Index(['ds', 't', 'trend', 'seasonal_lower', 'seasonal_upper', 'trend_lower',
           'trend_upper', 'yhat_lower', 'yhat_upper', 'weekly', 'weekly_lower',
           'weekly_upper', 'yearly', 'yearly_lower', 'yearly_upper', 'seasonal',
           'yhat', 'quantity'],
          dtype='object')




```python
## data ranges
minimum = min(model["ds"])
maximum = max(model["ds"])
missing_dates = model[np.isnan(model['quantity'])]
start_forecast = missing_dates.iloc[0]["ds"]
end_forecast = missing_dates.iloc[-1]["ds"]
minimum, maximum, start_forecast, end_forecast
```




    ('2014-02-04', '2015-01-28', '2014-10-31', '2015-01-28')




```python
## just in case, date conversion
model['ds'] = pd.to_datetime(model['ds'])
## dataframe info
#model.describe
model.dtypes
```




    ds                datetime64[ns]
    t                        float64
    trend                    float64
    seasonal_lower           float64
    seasonal_upper           float64
    trend_lower              float64
    trend_upper              float64
    yhat_lower               float64
    yhat_upper               float64
    weekly                   float64
    weekly_lower             float64
    weekly_upper             float64
    yearly                   float64
    yearly_lower             float64
    yearly_upper             float64
    seasonal                 float64
    yhat                     float64
    quantity                 float64
    dtype: object




```python
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')
```


```python
#prediction = model[['ds','yhat_lower','quantity','yhat_upper']]
#prediction = model[['ds','yhat_lower','quantity','yhat_upper','yhat','trend','trend_lower','trend_upper']]
prediction = model
prediction.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ds</th>
      <th>t</th>
      <th>trend</th>
      <th>seasonal_lower</th>
      <th>seasonal_upper</th>
      <th>trend_lower</th>
      <th>trend_upper</th>
      <th>yhat_lower</th>
      <th>yhat_upper</th>
      <th>weekly</th>
      <th>weekly_lower</th>
      <th>weekly_upper</th>
      <th>yearly</th>
      <th>yearly_lower</th>
      <th>yearly_upper</th>
      <th>seasonal</th>
      <th>yhat</th>
      <th>quantity</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2014-02-04</td>
      <td>0.000000</td>
      <td>11.898089</td>
      <td>-20.040099</td>
      <td>-20.040099</td>
      <td>11.898089</td>
      <td>11.898089</td>
      <td>-39.289951</td>
      <td>20.598438</td>
      <td>4.232679</td>
      <td>4.232679</td>
      <td>4.232679</td>
      <td>-24.272778</td>
      <td>-24.272778</td>
      <td>-24.272778</td>
      <td>-20.040099</td>
      <td>-8.142011</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2014-02-05</td>
      <td>0.003731</td>
      <td>12.228785</td>
      <td>-14.582592</td>
      <td>-14.582592</td>
      <td>12.228785</td>
      <td>12.228785</td>
      <td>-31.982350</td>
      <td>28.280061</td>
      <td>4.613898</td>
      <td>4.613898</td>
      <td>4.613898</td>
      <td>-19.196490</td>
      <td>-19.196490</td>
      <td>-19.196490</td>
      <td>-14.582592</td>
      <td>-2.353807</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2014-02-06</td>
      <td>0.007463</td>
      <td>12.559482</td>
      <td>-15.480227</td>
      <td>-15.480227</td>
      <td>12.559482</td>
      <td>12.559482</td>
      <td>-32.957724</td>
      <td>25.045041</td>
      <td>-0.721072</td>
      <td>-0.721072</td>
      <td>-0.721072</td>
      <td>-14.759154</td>
      <td>-14.759154</td>
      <td>-14.759154</td>
      <td>-15.480227</td>
      <td>-2.920745</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2014-02-07</td>
      <td>0.011194</td>
      <td>12.890179</td>
      <td>-19.730748</td>
      <td>-19.730748</td>
      <td>12.890179</td>
      <td>12.890179</td>
      <td>-37.711706</td>
      <td>24.259128</td>
      <td>-8.720244</td>
      <td>-8.720244</td>
      <td>-8.720244</td>
      <td>-11.010503</td>
      <td>-11.010503</td>
      <td>-11.010503</td>
      <td>-19.730748</td>
      <td>-6.840569</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2014-02-08</td>
      <td>0.014925</td>
      <td>13.220875</td>
      <td>-19.096363</td>
      <td>-19.096363</td>
      <td>13.220875</td>
      <td>13.220875</td>
      <td>-35.090737</td>
      <td>23.040418</td>
      <td>-11.118075</td>
      <td>-11.118075</td>
      <td>-11.118075</td>
      <td>-7.978288</td>
      <td>-7.978288</td>
      <td>-7.978288</td>
      <td>-19.096363</td>
      <td>-5.875488</td>
      <td>2.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
"""
prediction_test = prediction
prediction_test.index = list(prediction_test['ds'])
prediction_test.drop('ds',axis=1)
prediction_test = prediction_test.drop('ds', 1)
prediction_test.head()
#new_index = model['ds']
#prediction.set_index(new_index)
##my_plot = prediction.plot(figsize=(12,6))
##my_plot.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
"""
```




    "\nprediction_test = prediction\nprediction_test.index = list(prediction_test['ds'])\nprediction_test.drop('ds',axis=1)\nprediction_test = prediction_test.drop('ds', 1)\nprediction_test.head()\n#new_index = model['ds']\n#prediction.set_index(new_index)\n##my_plot = prediction.plot(figsize=(12,6))\n##my_plot.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))\n"




```python
## plot everything on one chart
xlabel='ds'
ylabel='quantity'
forecast_color = '#0072B2'
face_color = 'white'
fig = plt.figure(facecolor='w', figsize=(12, 6))
ax = fig.add_subplot(111)
ax.plot(prediction[xlabel].values, prediction[ylabel], 'k.')
ax.plot(prediction[xlabel].values, prediction['yhat'], ls='-', c=forecast_color)
ax.plot(prediction[xlabel].values, prediction['yhat_lower'], ls='-', c=face_color)
ax.plot(prediction[xlabel].values, prediction['yhat_upper'], ls='-', c=face_color)
ax.plot(prediction[xlabel].values, prediction['trend'], ls='-', c="orange")
ax.fill_between(
                prediction[xlabel].values, prediction['yhat_lower'], prediction['yhat_upper'],
                color=forecast_color, alpha=0.2)
ax.grid(True, which='major', c='gray', ls='-', lw=1, alpha=0.2)
plt.axvspan(start_forecast, end_forecast, color='red', alpha=0.1)
#ax.set_xlabel(xlabel)
ax.set_ylabel(ylabel)
ax.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
fig.tight_layout()
```


![png](output_10_0.png)



```python

```
